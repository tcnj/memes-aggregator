# Latest version of Erlang-based Elixir installation: https://hub.docker.com/_/elixir/
FROM elixir:latest

# Create and set home directory
ENV HOME /app
WORKDIR $HOME

RUN touch ~/.bashrc && curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
RUN ["/bin/bash", "-c", "source ~/.bashrc && nvm install v8.11.3"]

# Configure required environment
ENV MIX_ENV prod

# Set and expose PORT environmental variable
ENV PORT 5000
EXPOSE $PORT

# Install hex (Elixir package manager)
RUN mix local.hex --force

# Install rebar (Erlang build tool)
RUN mix local.rebar --force

# Copy all dependencies files
COPY mix.* ./

# Install all production dependencies
RUN mix deps.get --only prod

# Compile all dependencies
RUN mix deps.compile

# Copy all application files
COPY . .

# Compile the entire project
RUN ["/bin/bash", "-c", "source ~/.bashrc && nvm use v8.11.3 && mix compile && cd assets && npm install && npm run deploy && cd .. && mix phx.digest"]

# Run Ecto migrations and Phoenix server as an initial command
CMD mix do ecto.migrate, phx.server