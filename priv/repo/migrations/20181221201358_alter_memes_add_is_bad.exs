defmodule Memes.Repo.Migrations.AlterMemesAddIsBad do
  use Ecto.Migration

  def change do
    alter table(:memes) do
      add :is_bad, :boolean, default: false
    end
  end
end
