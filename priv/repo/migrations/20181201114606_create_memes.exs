defmodule Memes.Repo.Migrations.CreateMemes do
  use Ecto.Migration

  def change do
    create table(:memes) do
      add :title, :string
      add :image_url, :string
      add :source_url, :string
      add :score, :decimal

      timestamps()
    end

  end
end
