defmodule Memes.Repo.Migrations.AlterMemesChangeEverythingToText do
  use Ecto.Migration

  def change do
    alter table(:memes) do
      modify :image_url, :text
      modify :source_url, :text
    end
  end
end
