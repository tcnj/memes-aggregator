defmodule Memes.Repo.Migrations.CreateUnqieConstraintMemesSourceUrl do
  use Ecto.Migration

  def change do
    create unique_index(:memes, [:source_url])
  end
end
