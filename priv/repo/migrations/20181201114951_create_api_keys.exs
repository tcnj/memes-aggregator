defmodule Memes.Repo.Migrations.CreateApiKeys do
  use Ecto.Migration

  def change do
    create table(:api_keys) do
      add :key, :string

      timestamps()
    end

  end
end
