defmodule Memes.Repo.Migrations.AlterMemesChangeTitleToText do
  use Ecto.Migration

  def change do
    alter table(:memes) do
      modify :title, :text
    end
  end
end
