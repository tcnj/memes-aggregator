defmodule Memes.Repo.Migrations.AlterSourceSubRedditsAddDefaultSubscriptionFlag do
  use Ecto.Migration

  def change do
    alter table(:source_sub_reddits) do
      add :default_subscription, :boolean, default: false
    end
  end
end
