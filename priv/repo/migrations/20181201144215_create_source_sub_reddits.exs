defmodule Memes.Repo.Migrations.CreateSourceSubReddits do
  use Ecto.Migration

  def change do
    create table(:source_sub_reddits) do
      add :name, :string

      timestamps()
    end

    create unique_index(:source_sub_reddits, [:name])
  end
end
