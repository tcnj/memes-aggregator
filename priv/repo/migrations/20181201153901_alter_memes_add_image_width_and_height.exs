defmodule Memes.Repo.Migrations.AlterMemesAddImageWidthAndHeight do
  use Ecto.Migration

  def change do
    alter table(:memes) do
      add :image_width, :integer
      add :image_height, :integer
    end
  end
end
