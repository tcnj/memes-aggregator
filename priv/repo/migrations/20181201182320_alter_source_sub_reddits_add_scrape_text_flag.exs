defmodule Memes.Repo.Migrations.AlterSourceSubRedditsAddScrapeTextFlag do
  use Ecto.Migration

  def change do
    alter table(:source_sub_reddits) do
      add :scrape_text_posts, :boolean, default: false
    end
  end
end
