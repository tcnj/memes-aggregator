# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Memes.Repo.insert!(%Memes.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

if Memes.Back.list_api_keys() |> length < 1 do
  IO.puts "Creating key..."
  Memes.Back.create_api_key(%{"key" => "d@nkm3m3s"})
else
  IO.puts "Not creating key as one already exists."
end