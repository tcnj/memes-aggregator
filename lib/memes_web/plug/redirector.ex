defmodule MemesWeb.Plug.Redirector do
  @redirects %{"/" => "/s"}

  import Plug.Conn
  import Phoenix.Controller

  def init(_), do: @redirects

  def call(conn, map) do
    path = "/" <> Enum.join(conn.path_info, "/")
    to_url = map[path]

    if to_url do
      conn |> redirect(to: to_url) |> halt()
    else
      conn
    end
  end
end