defmodule MemesWeb.Plug.LikeWWW do
  import Plug.Conn
  import Phoenix.Controller

  def init(like_it), do: like_it

  def call(conn, like_it) do
    has_www = String.slice(conn.host, 0..3) == "www."
    {action, domain} = cond do
      like_it and not has_www ->
        {:redirect, "www." <> conn.host}
      not like_it and has_www ->
        {:redirect, String.slice(conn.host, 4..-1)}
      true ->
        {:none, nil}
    end

    if action == :redirect do
      port_portion = if(conn.port != 80, do: ":" <> to_string(conn.port), else: "")
      query_portion = if(String.length(conn.query_string) > 0, do: "?" <> conn.query_string, else: "")
      conn |> redirect(external: to_string(conn.scheme) <> "://" <> domain <> port_portion <> conn.request_path <> query_portion) |> halt()
    else
      conn
    end
  end
end