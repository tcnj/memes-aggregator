defmodule MemesWeb.MemeView do
  use MemesWeb, :view

  def render("stats.txt", %{memes: memes}) do
    memes
    |> Enum.map(&(&1.score))
    |> Enum.join("\n")
  end
end
