defmodule MemesWeb.SourceSubRedditView do
  use MemesWeb, :view

  def render("index.json", %{source_sub_reddits: source_sub_reddits}) do
    source_sub_reddits |> Enum.map(fn source ->
      %{
        name: source.name,
        scrape_text_posts: source.scrape_text_posts,
        default_subscription: source.default_subscription
      }
    end)
  end
end
