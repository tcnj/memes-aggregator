defmodule MemesWeb.AuthenticationController do
  use MemesWeb, :controller

  def authenticate(conn, params) do
    conn
    |> put_session(:api_key, params["key"])
    |> send_resp(200, "Updated session")
  end
end