defmodule MemesWeb.MemePageController do
  use MemesWeb, :controller

  alias Memes.Repo
  alias Memes.Front.Meme

  import Ecto.Query

  @memes_initial 20
  @memes_extra 20

  def memes(conn, _params) do
    memes = query() |> Repo.all()
    put_layout(conn, false)
    |> render("memes.html", memes: memes)
  end

  def more_memes(conn, %{"offset" => offset}) do
    memes = query(offset) |> Repo.all()

    put_layout(conn, false)
    |> render("some_memes.html", memes: memes)
  end

  defp query(offset \\ 0) do
    num_memes = if offset == 0, do: @memes_initial, else: @memes_extra
    from m in Meme, where: m.is_bad == false, order_by: [desc: m.score], limit: ^num_memes, offset: ^offset
  end
end