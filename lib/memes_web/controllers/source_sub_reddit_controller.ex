defmodule MemesWeb.SourceSubRedditController do
  use MemesWeb, :controller

  alias Memes.Back
  alias Memes.Back.SourceSubReddit

  def index(conn, _params) do
    source_sub_reddits = Back.list_source_sub_reddits()
    render(conn, :index, source_sub_reddits: source_sub_reddits)
  end

  def new(conn, _params) do
    changeset = Back.change_source_sub_reddit(%SourceSubReddit{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"source_sub_reddit" => source_sub_reddit_params}) do
    case Back.create_source_sub_reddit(source_sub_reddit_params) do
      {:ok, source_sub_reddit} ->
        conn
        |> put_flash(:info, "Source sub reddit created successfully.")
        |> redirect(to: source_sub_reddit_path(conn, :show, source_sub_reddit))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    source_sub_reddit = Back.get_source_sub_reddit!(id)
    render(conn, "show.html", source_sub_reddit: source_sub_reddit)
  end

  def edit(conn, %{"id" => id}) do
    source_sub_reddit = Back.get_source_sub_reddit!(id)
    changeset = Back.change_source_sub_reddit(source_sub_reddit)
    render(conn, "edit.html", source_sub_reddit: source_sub_reddit, changeset: changeset)
  end

  def update(conn, %{"id" => id, "source_sub_reddit" => source_sub_reddit_params}) do
    source_sub_reddit = Back.get_source_sub_reddit!(id)

    case Back.update_source_sub_reddit(source_sub_reddit, source_sub_reddit_params) do
      {:ok, source_sub_reddit} ->
        conn
        |> put_flash(:info, "Source sub reddit updated successfully.")
        |> redirect(to: source_sub_reddit_path(conn, :show, source_sub_reddit))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", source_sub_reddit: source_sub_reddit, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    source_sub_reddit = Back.get_source_sub_reddit!(id)
    {:ok, _source_sub_reddit} = Back.delete_source_sub_reddit(source_sub_reddit)

    conn
    |> put_flash(:info, "Source sub reddit deleted successfully.")
    |> redirect(to: source_sub_reddit_path(conn, :index))
  end
end
