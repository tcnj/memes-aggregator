defmodule MemesWeb.PageController do
  use MemesWeb, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
