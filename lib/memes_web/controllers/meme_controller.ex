defmodule MemesWeb.MemeController do
  use MemesWeb, :controller

  alias Memes.Front
  alias Memes.Front.Meme

  def index(conn, _params) do
    memes = Front.list_memes()
    render(conn, "index.html", memes: memes)
  end

  def new(conn, _params) do
    changeset = Front.change_meme(%Meme{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"meme" => meme_params}) do
    case Front.create_meme(meme_params) do
      {:ok, meme} ->
        conn
        |> put_flash(:info, "Meme created successfully.")
        |> redirect(to: meme_path(conn, :show, meme))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    meme = Front.get_meme!(id)
    render(conn, "show.html", meme: meme)
  end

  def edit(conn, %{"id" => id}) do
    meme = Front.get_meme!(id)
    changeset = Front.change_meme(meme)
    render(conn, "edit.html", meme: meme, changeset: changeset)
  end

  def update(conn, %{"id" => id, "meme" => meme_params}) do
    meme = Front.get_meme!(id)

    case Front.update_meme(meme, meme_params) do
      {:ok, meme} ->
        conn
        |> put_flash(:info, "Meme updated successfully.")
        |> redirect(to: meme_path(conn, :show, meme))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", meme: meme, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    meme = Front.get_meme!(id)
    {:ok, _meme} = Front.delete_meme(meme)

    conn
    |> put_flash(:info, "Meme deleted successfully.")
    |> redirect(to: meme_path(conn, :index))
  end

  def stats(conn, _params) do
    memes = Front.list_memes()
    render(conn, "stats.txt", memes: memes)
  end
end
