defmodule MemesWeb.APIKeyController do
  use MemesWeb, :controller

  alias Memes.Back
  alias Memes.Back.APIKey

  def index(conn, _params) do
    api_keys = Back.list_api_keys()
    render(conn, "index.html", api_keys: api_keys)
  end

  def new(conn, _params) do
    changeset = Back.change_api_key(%APIKey{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"api_key" => api_key_params}) do
    case Back.create_api_key(api_key_params) do
      {:ok, api_key} ->
        conn
        |> put_flash(:info, "Api key created successfully.")
        |> redirect(to: api_key_path(conn, :show, api_key))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    api_key = Back.get_api_key!(id)
    render(conn, "show.html", api_key: api_key)
  end

  def edit(conn, %{"id" => id}) do
    api_key = Back.get_api_key!(id)
    changeset = Back.change_api_key(api_key)
    render(conn, "edit.html", api_key: api_key, changeset: changeset)
  end

  def update(conn, %{"id" => id, "api_key" => api_key_params}) do
    api_key = Back.get_api_key!(id)

    case Back.update_api_key(api_key, api_key_params) do
      {:ok, api_key} ->
        conn
        |> put_flash(:info, "Api key updated successfully.")
        |> redirect(to: api_key_path(conn, :show, api_key))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", api_key: api_key, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    api_key = Back.get_api_key!(id)
    {:ok, _api_key} = Back.delete_api_key(api_key)

    conn
    |> put_flash(:info, "Api key deleted successfully.")
    |> redirect(to: api_key_path(conn, :index))
  end
end
