defmodule MemesWeb.MemeInController do
  use MemesWeb, :controller

  alias Memes.Front
  alias Memes.Repo

  import Ecto.Query

  def post(conn, params) do
    source_url = params["source_url"]

    Repo.delete_all(from a in Front.Meme, where: a.source_url == ^source_url)

    case Front.create_meme(params) do
      {:ok, _meme} ->
        conn
        |> send_resp(200, "Good!")
      {:error, %Ecto.Changeset{} = changeset} ->
        IO.inspect changeset
        IO.inspect params
        conn
        |> send_resp(400, "Bad!")
    end
  end

  def check_in(conn, params) do
    Repo.delete_all(from m in Front.Meme, where: m.updated_at < ^NaiveDateTime.add(NaiveDateTime.utc_now, -30 * 60))

    conn |> send_resp(200, "Good!")
  end
end