defmodule MemesWeb.Router do
  use MemesWeb, :router
  import Plug.Conn

  alias Memes.Back

  def authenticate(conn, _) do
    conn = fetch_query_params(conn)

    if (conn.params["key"] && Back.is_api_key!(conn.params["key"])) || (get_session(conn, :api_key) && Back.is_api_key!(get_session(conn, :api_key))) do
      conn
    else
      conn |> send_resp(401, "Provide API key") |> halt()
    end
  end

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug :fetch_session
    plug :fetch_flash
  end

  pipeline :auth do
    plug :authenticate, nil
  end

  scope "/", MemesWeb do
    pipe_through :browser # Use the default browser stack

    get "/s", MemePageController, :memes
    get "/front/more_memes", MemePageController, :more_memes

    get "/back/authenticate", AuthenticationController, :authenticate
  end

  scope "/", MemesWeb do
    pipe_through :browser
    pipe_through :auth

    get "/front/memes/stats", MemeController, :stats

    resources "/front/memes", MemeController
    resources "/back/api_keys", APIKeyController
    resources "/back/source_sub_reddits", SourceSubRedditController
  end

  scope "/", MemesWeb do
    pipe_through :api
    pipe_through :auth

    post "/back/meme_in", MemeInController, :post
    get "/back/source_sub_reddits.json", SourceSubRedditController, :index
    get "/back/scrape_check_in", MemeInController, :check_in
  end
end
