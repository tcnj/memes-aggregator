defmodule Memes.Back.SourceSubReddit do
  use Ecto.Schema
  import Ecto.Changeset


  schema "source_sub_reddits" do
    field :name, :string
    field :scrape_text_posts, :boolean
    field :default_subscription, :boolean

    timestamps()
  end

  @doc false
  def changeset(source_sub_reddit, attrs) do
    source_sub_reddit
    |> cast(attrs, [:name, :scrape_text_posts, :default_subscription])
    |> validate_required([:name])
    |> unique_constraint(:name)
  end
end
