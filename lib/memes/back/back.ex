defmodule Memes.Back do
  @moduledoc """
  The Back context.
  """

  import Ecto.Query, warn: false
  alias Memes.Repo

  alias Memes.Back.APIKey

  @doc """
  Returns the list of api_keys.

  ## Examples

      iex> list_api_keys()
      [%APIKey{}, ...]

  """
  def list_api_keys do
    Repo.all(APIKey)
  end

  @doc """
  Gets a single api_key.

  Raises `Ecto.NoResultsError` if the Api key does not exist.

  ## Examples

      iex> get_api_key!(123)
      %APIKey{}

      iex> get_api_key!(456)
      ** (Ecto.NoResultsError)

  """
  def get_api_key!(id), do: Repo.get!(APIKey, id)



  def is_api_key!(key), do: length(Repo.all(from a in APIKey, where: a.key == ^key)) > 0

  @doc """
  Creates a api_key.

  ## Examples

      iex> create_api_key(%{field: value})
      {:ok, %APIKey{}}

      iex> create_api_key(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_api_key(attrs \\ %{}) do
    %APIKey{}
    |> APIKey.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a api_key.

  ## Examples

      iex> update_api_key(api_key, %{field: new_value})
      {:ok, %APIKey{}}

      iex> update_api_key(api_key, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_api_key(%APIKey{} = api_key, attrs) do
    api_key
    |> APIKey.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a APIKey.

  ## Examples

      iex> delete_api_key(api_key)
      {:ok, %APIKey{}}

      iex> delete_api_key(api_key)
      {:error, %Ecto.Changeset{}}

  """
  def delete_api_key(%APIKey{} = api_key) do
    Repo.delete(api_key)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking api_key changes.

  ## Examples

      iex> change_api_key(api_key)
      %Ecto.Changeset{source: %APIKey{}}

  """
  def change_api_key(%APIKey{} = api_key) do
    APIKey.changeset(api_key, %{})
  end

  alias Memes.Back.SourceSubReddit

  @doc """
  Returns the list of source_sub_reddits.

  ## Examples

      iex> list_source_sub_reddits()
      [%SourceSubReddit{}, ...]

  """
  def list_source_sub_reddits do
    Repo.all(from s in SourceSubReddit, order_by: [asc: s.name])
  end

  @doc """
  Gets a single source_sub_reddit.

  Raises `Ecto.NoResultsError` if the Source sub reddit does not exist.

  ## Examples

      iex> get_source_sub_reddit!(123)
      %SourceSubReddit{}

      iex> get_source_sub_reddit!(456)
      ** (Ecto.NoResultsError)

  """
  def get_source_sub_reddit!(id), do: Repo.get!(SourceSubReddit, id)

  @doc """
  Creates a source_sub_reddit.

  ## Examples

      iex> create_source_sub_reddit(%{field: value})
      {:ok, %SourceSubReddit{}}

      iex> create_source_sub_reddit(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_source_sub_reddit(attrs \\ %{}) do
    %SourceSubReddit{}
    |> SourceSubReddit.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a source_sub_reddit.

  ## Examples

      iex> update_source_sub_reddit(source_sub_reddit, %{field: new_value})
      {:ok, %SourceSubReddit{}}

      iex> update_source_sub_reddit(source_sub_reddit, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_source_sub_reddit(%SourceSubReddit{} = source_sub_reddit, attrs) do
    source_sub_reddit
    |> SourceSubReddit.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a SourceSubReddit.

  ## Examples

      iex> delete_source_sub_reddit(source_sub_reddit)
      {:ok, %SourceSubReddit{}}

      iex> delete_source_sub_reddit(source_sub_reddit)
      {:error, %Ecto.Changeset{}}

  """
  def delete_source_sub_reddit(%SourceSubReddit{} = source_sub_reddit) do
    Repo.delete(source_sub_reddit)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking source_sub_reddit changes.

  ## Examples

      iex> change_source_sub_reddit(source_sub_reddit)
      %Ecto.Changeset{source: %SourceSubReddit{}}

  """
  def change_source_sub_reddit(%SourceSubReddit{} = source_sub_reddit) do
    SourceSubReddit.changeset(source_sub_reddit, %{})
  end
end
