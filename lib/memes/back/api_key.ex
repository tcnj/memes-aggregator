defmodule Memes.Back.APIKey do
  use Ecto.Schema
  import Ecto.Changeset


  schema "api_keys" do
    field :key, :string

    timestamps()
  end

  @doc false
  def changeset(api_key, attrs) do
    api_key
    |> cast(attrs, [:key])
    |> validate_required([:key])
  end
end
