defmodule Memes.Front.Meme do
  use Ecto.Schema
  import Ecto.Changeset


  schema "memes" do
    field :image_url, :string
    field :score, :decimal
    field :source_url, :string
    field :title, :string

    field :image_width, :integer
    field :image_height, :integer

    field :is_bad, :boolean

    timestamps()
  end

  @doc false
  def changeset(meme, attrs) do
    meme
    |> cast(attrs, [:title, :image_url, :source_url, :score, :image_width, :image_height, :is_bad])
    |> validate_required([:title, :source_url, :score])
    |> unique_constraint(:source_url)
  end
end
