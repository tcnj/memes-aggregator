from tesserocr import image_to_text
from PIL import Image
from io import BytesIO
import requests

bad_patterns_vowelless = [
    'fck', 'sht', 'btch', 'cnt', 'wtf'
]

bad_patterns = [
    'dick', 'sex', 'ass', 'arse'
]

remove_patterns = [
    " ", "\n", "*", ".", "?", "#", "!", "@"
]

vowels = ["a", "e", "i", "o", "u"]


def is_bad(text):
    for pat in remove_patterns:
        text = text.replace(pat, "")

    for pat in bad_patterns:
        i = text.find(pat)
        if i > -1:
            # print("A %i" % i)
            # print(text)
            return True
        
    # print(text)

    for pat in vowels:
        text = text.replace(pat, "")
    
    # print(text)

    for pat in bad_patterns_vowelless:
        i = text.find(pat)
        if i > -1:
            # print("B %i" % i)
            # print(text)
            return True
    
    return False

def is_bad_image(url):
    r = requests.get(url, stream=True)
    if r.status_code == 200:
        buf = bytes()
        for chunk in r.iter_content(128):
            buf += chunk
        
        img = Image.open(BytesIO(buf))

        text = image_to_text(img).lower()

        return is_bad(text)
    return True

if __name__ == '__main__':
    import sys
    print(is_bad_image(sys.argv[1]))
