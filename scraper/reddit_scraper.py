import sys
import requests
from bs4 import BeautifulSoup
import time
from settings import *
from death import is_bad_image, is_bad

image_extentions = [".jpg", ".gif", ".png"]

reddit_base_url = "http://www.reddit.com"
headers = {
    'User-Agent': 'Mozilla/5.0 (Android 4.4; Mobile; rv:41.0) Gecko/41.0 Firefox/41.0',
}
DEBUG = False

meme_dict = {}

def get_sub_reddits(url):
    r = requests.get(url)
    reddits = []
    for sub in r.json():
        reddits.append({
            "path": "/r/"+sub["name"],
            "text": sub["scrape_text_posts"],
            "default": sub["default_subscription"],
        })

    return reddits


def get_memes(base_url, paths, extention, headers, memes, memes_per_path):
    for pathh in paths:
        path = pathh["path"]
        text = pathh["text"]
        default = pathh["default"]
        memes[path] = {
            "subscribers": 1000,
            "memes": [],
        }
        print(f"\t{path}", end=" ")
        try:
            r_about = requests.get(base_url+path+"/about"+extention, headers=headers)
            subscribers = r_about.json()["data"]["subscribers"]
            memes[path]["subscribers"] = int(subscribers)

            r = requests.get(base_url+path+extention+"?limit="+memes_per_path, headers=headers)

            json = r.json()
            if json.get("error", 0) == 429:
                print("too many requests")
                sys.exit()
            for i in range(len(json["data"]["children"])):
                meme = json["data"]["children"][i]["data"]
                image = meme.get("thumbnail_width", None) != None
                if ((image and meme["url"][-4:] in image_extentions) or text) and meme["over_18"] == False and meme["stickied"] == False:
                    title = meme["title"]
                    upvotes = meme["ups"]
                    link = base_url + meme["permalink"]
                    score = int(upvotes)/(subscribers**0.5)
                    if default:
                        score *= 5
                    meme_dict = {
                            "title": title,
                            "score": str(score),
                            "source_url": link,
                        }
                    if image:
                        image = meme["preview"]["images"][0]["resolutions"][-1]["url"].replace("&amp;", "&")
                        width = meme["preview"]["images"][0]["source"]["width"]
                        height = meme["preview"]["images"][0]["source"]["height"]

                        meme_dict.update({
                            "image_url": image,
                            "image_width": width,
                            "image_height": height,
                        })


                    memes[path]["memes"].append(meme_dict)

        except Exception as e:
            print("meme threw error")
            print(f"ERROR: {e}")
            print(path)
        print(f"complete")

def send_memes(memes_to_send):
    i = 0
    for path in memes_to_send:
        print(f"\t{path}", end=" ")
        for meme in memes_to_send[path]["memes"]:
            i += 1
            bad = is_bad(meme["title"]) or is_bad_image(meme["image_url"])
            meme.update({"is_bad": str(bad).lower()})
            res = requests.post(post_url, data=meme)
        print(f"complete")
        print(i)

def get_and_send():
    print("getting subreddits...")
    sub_reddits = get_sub_reddits(get_subs_url)
    print("getting memes....")
    get_memes(reddit_base_url, sub_reddits, ".json", headers, meme_dict, "100")
    print("got memes")
    print("sending memes...")
    send_memes(meme_dict)
    print("sent memes...")

if __name__ == "__main__":
    while True:
        get_and_send()
        print("waiting to get more memes...")
        time.sleep(60*2)
