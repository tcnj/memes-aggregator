from dotenv import load_dotenv
import os
load_dotenv()


ELIXIR_API_KEY = os.getenv("ELIXIR_API_KEY")
IMGUR_API_KEY = os.getenv("IMGUR_API_KEY")
POST_URL = os.getenv("POST_URL")
GET_SUBS_URL = os.getenv("GET_SUBS_URL")
MEME_SEND_SUCCESS_URL = os.getenv("MEME_SEND_SUCCESS_URL")


post_url = POST_URL+ELIXIR_API_KEY
get_subs_url = GET_SUBS_URL+ELIXIR_API_KEY
