import reddit_scraper
import imgur_scraper
import time
import requests
from settings import *

sleep_time = 60*2


def send_success():
    requests.get(MEME_SEND_SUCCESS_URL+ELIXIR_API_KEY)


while True:
    reddit_scraper.get_and_send()
    imgur_scraper.get_and_send()
    send_success()

    time.sleep(sleep_time)

