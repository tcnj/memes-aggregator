import requests
import os
import urllib.request
from PIL import Image
import random

base = "https://xkcd.com/%s/info.0.json"
thing = "https://www.xkcd.com/"
i = 0
while True:
    i += 1
    r = requests.get(base % i)
    if r.status_code != 200 and i != 404:
        break
    if i == 404:
        continue
    data = r.json()
    image_url = data["img"]
    source_url = thing + str(i)
    title = "XKCD - " + data["safe_title"]
    score = 5/random.randint(1, 53) * 10
    urllib.request.urlretrieve(image_url, "xkcd"+str(i)+".png")
    im = Image.open("xkcd"+str(i)+".png")
    width, height = im.size
    os.remove("xkcd"+str(i)+".png")
    print(title, i, score)

    res = requests.post(post_url, data={
            "title": title,
            "image_url": image_url,
            "score": score,
            "source_url": source_url,
            "image_width": width,
            "image_height": height,
        })
