# Meme Scrapper
This directory contains a variory of bots that srcape / get memes from several sources including

* Various Subreddits
* Imgur
* Tumblr
* More comming soon

## Purpose of a bot
A bots job is to find memes and then rank them using the meme rank alogrithm. After all necersery data has been collected (width, height, img_url, source_url, meme_rank, etc) the infomation is then sent to a webserver is an HTTP post request.

## Usage
run main.py and that will get memes from reddit and imgur

## Environment Variables
Variable | Use
---------|-----
`ELIXIR_API_KEY` | api key for elixir webserver
`IMGUR_API_KEY` | client_id generated when you make an imgur account
`POST_URL` | url to post memes to (part of elixir server). URL must end with `?key=`
`GET_SUBS_URL` | url to get sub reddits to scrape. URl must end with `?key=`
`MEME_SEND_SUCCESS_URL` | url to send "meme sending complete" message to. URL must end with `?key=`
