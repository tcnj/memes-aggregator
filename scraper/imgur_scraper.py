import requests
from bs4 import BeautifulSoup
from settings import *
from death import is_bad

image_extentions = [".jpg", ".gif", ".png"]



headers = {
    'User-Agent': 'Mozilla/5.0 (Android 4.4; Mobile; rv:41.0) Gecko/41.0 Firefox/41.0',
}
url = "https://api.imgur.com/3/gallery/hot/top/month"
args = "?showViral=true&mature=false&album_previews=true"


headers = {
    'Authorization': f'Client-ID {IMGUR_API_KEY}'
}
memes = []

def get_and_send():
    for i in range(1, 5):
        r = requests.get(url+"/page"+str(i)+args, headers=headers)
        data = r.json()["data"]
        for meme in data:
            meme_url = meme["link"]
            if meme_url.split("/")[-1].find(".") == -1:
                meme_r = requests.get(meme_url)
                html = meme_r.text
                soup = BeautifulSoup(html, "html.parser")
                image = soup.find("link", {"rel": "image_src"})
                if image:
                    link = image["href"]
                    height = meme["cover_height"]
                    width = meme["cover_width"]
                    title = meme["title"]
                    score = ((meme["ups"]**0.5 * 0.02 )/200)*40
                    print(title, score, link, height, width)

                    bad = is_bad_image(link)

                    res = requests.post(post_url, data={
                            "title": title,
                            "image_url": link,
                            "score": score,
                            "source_url": link[:-4],
                            "image_width": width,
                            "image_height": height,
                            "is_bad": bad
                        })

