defmodule MemesWeb.MemeControllerTest do
  use MemesWeb.ConnCase

  alias Memes.Front

  @create_attrs %{image_url: "some image_url", score: "120.5", source_url: "some source_url", title: "some title"}
  @update_attrs %{image_url: "some updated image_url", score: "456.7", source_url: "some updated source_url", title: "some updated title"}
  @invalid_attrs %{image_url: nil, score: nil, source_url: nil, title: nil}

  def fixture(:meme) do
    {:ok, meme} = Front.create_meme(@create_attrs)
    meme
  end

  describe "index" do
    test "lists all memes", %{conn: conn} do
      conn = get conn, meme_path(conn, :index)
      assert html_response(conn, 200) =~ "Listing Memes"
    end
  end

  describe "new meme" do
    test "renders form", %{conn: conn} do
      conn = get conn, meme_path(conn, :new)
      assert html_response(conn, 200) =~ "New Meme"
    end
  end

  describe "create meme" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post conn, meme_path(conn, :create), meme: @create_attrs

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == meme_path(conn, :show, id)

      conn = get conn, meme_path(conn, :show, id)
      assert html_response(conn, 200) =~ "Show Meme"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, meme_path(conn, :create), meme: @invalid_attrs
      assert html_response(conn, 200) =~ "New Meme"
    end
  end

  describe "edit meme" do
    setup [:create_meme]

    test "renders form for editing chosen meme", %{conn: conn, meme: meme} do
      conn = get conn, meme_path(conn, :edit, meme)
      assert html_response(conn, 200) =~ "Edit Meme"
    end
  end

  describe "update meme" do
    setup [:create_meme]

    test "redirects when data is valid", %{conn: conn, meme: meme} do
      conn = put conn, meme_path(conn, :update, meme), meme: @update_attrs
      assert redirected_to(conn) == meme_path(conn, :show, meme)

      conn = get conn, meme_path(conn, :show, meme)
      assert html_response(conn, 200) =~ "some updated image_url"
    end

    test "renders errors when data is invalid", %{conn: conn, meme: meme} do
      conn = put conn, meme_path(conn, :update, meme), meme: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit Meme"
    end
  end

  describe "delete meme" do
    setup [:create_meme]

    test "deletes chosen meme", %{conn: conn, meme: meme} do
      conn = delete conn, meme_path(conn, :delete, meme)
      assert redirected_to(conn) == meme_path(conn, :index)
      assert_error_sent 404, fn ->
        get conn, meme_path(conn, :show, meme)
      end
    end
  end

  defp create_meme(_) do
    meme = fixture(:meme)
    {:ok, meme: meme}
  end
end
