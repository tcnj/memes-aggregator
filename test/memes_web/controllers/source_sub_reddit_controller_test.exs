defmodule MemesWeb.SourceSubRedditControllerTest do
  use MemesWeb.ConnCase

  alias Memes.Back

  @create_attrs %{name: "some name"}
  @update_attrs %{name: "some updated name"}
  @invalid_attrs %{name: nil}

  def fixture(:source_sub_reddit) do
    {:ok, source_sub_reddit} = Back.create_source_sub_reddit(@create_attrs)
    source_sub_reddit
  end

  describe "index" do
    test "lists all source_sub_reddits", %{conn: conn} do
      conn = get conn, source_sub_reddit_path(conn, :index)
      assert html_response(conn, 200) =~ "Listing Source sub reddits"
    end
  end

  describe "new source_sub_reddit" do
    test "renders form", %{conn: conn} do
      conn = get conn, source_sub_reddit_path(conn, :new)
      assert html_response(conn, 200) =~ "New Source sub reddit"
    end
  end

  describe "create source_sub_reddit" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post conn, source_sub_reddit_path(conn, :create), source_sub_reddit: @create_attrs

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == source_sub_reddit_path(conn, :show, id)

      conn = get conn, source_sub_reddit_path(conn, :show, id)
      assert html_response(conn, 200) =~ "Show Source sub reddit"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post conn, source_sub_reddit_path(conn, :create), source_sub_reddit: @invalid_attrs
      assert html_response(conn, 200) =~ "New Source sub reddit"
    end
  end

  describe "edit source_sub_reddit" do
    setup [:create_source_sub_reddit]

    test "renders form for editing chosen source_sub_reddit", %{conn: conn, source_sub_reddit: source_sub_reddit} do
      conn = get conn, source_sub_reddit_path(conn, :edit, source_sub_reddit)
      assert html_response(conn, 200) =~ "Edit Source sub reddit"
    end
  end

  describe "update source_sub_reddit" do
    setup [:create_source_sub_reddit]

    test "redirects when data is valid", %{conn: conn, source_sub_reddit: source_sub_reddit} do
      conn = put conn, source_sub_reddit_path(conn, :update, source_sub_reddit), source_sub_reddit: @update_attrs
      assert redirected_to(conn) == source_sub_reddit_path(conn, :show, source_sub_reddit)

      conn = get conn, source_sub_reddit_path(conn, :show, source_sub_reddit)
      assert html_response(conn, 200) =~ "some updated name"
    end

    test "renders errors when data is invalid", %{conn: conn, source_sub_reddit: source_sub_reddit} do
      conn = put conn, source_sub_reddit_path(conn, :update, source_sub_reddit), source_sub_reddit: @invalid_attrs
      assert html_response(conn, 200) =~ "Edit Source sub reddit"
    end
  end

  describe "delete source_sub_reddit" do
    setup [:create_source_sub_reddit]

    test "deletes chosen source_sub_reddit", %{conn: conn, source_sub_reddit: source_sub_reddit} do
      conn = delete conn, source_sub_reddit_path(conn, :delete, source_sub_reddit)
      assert redirected_to(conn) == source_sub_reddit_path(conn, :index)
      assert_error_sent 404, fn ->
        get conn, source_sub_reddit_path(conn, :show, source_sub_reddit)
      end
    end
  end

  defp create_source_sub_reddit(_) do
    source_sub_reddit = fixture(:source_sub_reddit)
    {:ok, source_sub_reddit: source_sub_reddit}
  end
end
