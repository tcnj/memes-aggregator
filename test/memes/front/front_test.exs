defmodule Memes.FrontTest do
  use Memes.DataCase

  alias Memes.Front

  describe "memes" do
    alias Memes.Front.Meme

    @valid_attrs %{image_url: "some image_url", score: "120.5", source_url: "some source_url", title: "some title"}
    @update_attrs %{image_url: "some updated image_url", score: "456.7", source_url: "some updated source_url", title: "some updated title"}
    @invalid_attrs %{image_url: nil, score: nil, source_url: nil, title: nil}

    def meme_fixture(attrs \\ %{}) do
      {:ok, meme} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Front.create_meme()

      meme
    end

    test "list_memes/0 returns all memes" do
      meme = meme_fixture()
      assert Front.list_memes() == [meme]
    end

    test "get_meme!/1 returns the meme with given id" do
      meme = meme_fixture()
      assert Front.get_meme!(meme.id) == meme
    end

    test "create_meme/1 with valid data creates a meme" do
      assert {:ok, %Meme{} = meme} = Front.create_meme(@valid_attrs)
      assert meme.image_url == "some image_url"
      assert meme.score == Decimal.new("120.5")
      assert meme.source_url == "some source_url"
      assert meme.title == "some title"
    end

    test "create_meme/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Front.create_meme(@invalid_attrs)
    end

    test "update_meme/2 with valid data updates the meme" do
      meme = meme_fixture()
      assert {:ok, meme} = Front.update_meme(meme, @update_attrs)
      assert %Meme{} = meme
      assert meme.image_url == "some updated image_url"
      assert meme.score == Decimal.new("456.7")
      assert meme.source_url == "some updated source_url"
      assert meme.title == "some updated title"
    end

    test "update_meme/2 with invalid data returns error changeset" do
      meme = meme_fixture()
      assert {:error, %Ecto.Changeset{}} = Front.update_meme(meme, @invalid_attrs)
      assert meme == Front.get_meme!(meme.id)
    end

    test "delete_meme/1 deletes the meme" do
      meme = meme_fixture()
      assert {:ok, %Meme{}} = Front.delete_meme(meme)
      assert_raise Ecto.NoResultsError, fn -> Front.get_meme!(meme.id) end
    end

    test "change_meme/1 returns a meme changeset" do
      meme = meme_fixture()
      assert %Ecto.Changeset{} = Front.change_meme(meme)
    end
  end
end
