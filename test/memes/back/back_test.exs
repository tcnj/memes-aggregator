defmodule Memes.BackTest do
  use Memes.DataCase

  alias Memes.Back

  describe "api_keys" do
    alias Memes.Back.APIKey

    @valid_attrs %{key: "some key"}
    @update_attrs %{key: "some updated key"}
    @invalid_attrs %{key: nil}

    def api_key_fixture(attrs \\ %{}) do
      {:ok, api_key} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Back.create_api_key()

      api_key
    end

    test "list_api_keys/0 returns all api_keys" do
      api_key = api_key_fixture()
      assert Back.list_api_keys() == [api_key]
    end

    test "get_api_key!/1 returns the api_key with given id" do
      api_key = api_key_fixture()
      assert Back.get_api_key!(api_key.id) == api_key
    end

    test "create_api_key/1 with valid data creates a api_key" do
      assert {:ok, %APIKey{} = api_key} = Back.create_api_key(@valid_attrs)
      assert api_key.key == "some key"
    end

    test "create_api_key/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Back.create_api_key(@invalid_attrs)
    end

    test "update_api_key/2 with valid data updates the api_key" do
      api_key = api_key_fixture()
      assert {:ok, api_key} = Back.update_api_key(api_key, @update_attrs)
      assert %APIKey{} = api_key
      assert api_key.key == "some updated key"
    end

    test "update_api_key/2 with invalid data returns error changeset" do
      api_key = api_key_fixture()
      assert {:error, %Ecto.Changeset{}} = Back.update_api_key(api_key, @invalid_attrs)
      assert api_key == Back.get_api_key!(api_key.id)
    end

    test "delete_api_key/1 deletes the api_key" do
      api_key = api_key_fixture()
      assert {:ok, %APIKey{}} = Back.delete_api_key(api_key)
      assert_raise Ecto.NoResultsError, fn -> Back.get_api_key!(api_key.id) end
    end

    test "change_api_key/1 returns a api_key changeset" do
      api_key = api_key_fixture()
      assert %Ecto.Changeset{} = Back.change_api_key(api_key)
    end
  end

  describe "source_sub_reddits" do
    alias Memes.Back.SourceSubReddit

    @valid_attrs %{name: "some name"}
    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    def source_sub_reddit_fixture(attrs \\ %{}) do
      {:ok, source_sub_reddit} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Back.create_source_sub_reddit()

      source_sub_reddit
    end

    test "list_source_sub_reddits/0 returns all source_sub_reddits" do
      source_sub_reddit = source_sub_reddit_fixture()
      assert Back.list_source_sub_reddits() == [source_sub_reddit]
    end

    test "get_source_sub_reddit!/1 returns the source_sub_reddit with given id" do
      source_sub_reddit = source_sub_reddit_fixture()
      assert Back.get_source_sub_reddit!(source_sub_reddit.id) == source_sub_reddit
    end

    test "create_source_sub_reddit/1 with valid data creates a source_sub_reddit" do
      assert {:ok, %SourceSubReddit{} = source_sub_reddit} = Back.create_source_sub_reddit(@valid_attrs)
      assert source_sub_reddit.name == "some name"
    end

    test "create_source_sub_reddit/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Back.create_source_sub_reddit(@invalid_attrs)
    end

    test "update_source_sub_reddit/2 with valid data updates the source_sub_reddit" do
      source_sub_reddit = source_sub_reddit_fixture()
      assert {:ok, source_sub_reddit} = Back.update_source_sub_reddit(source_sub_reddit, @update_attrs)
      assert %SourceSubReddit{} = source_sub_reddit
      assert source_sub_reddit.name == "some updated name"
    end

    test "update_source_sub_reddit/2 with invalid data returns error changeset" do
      source_sub_reddit = source_sub_reddit_fixture()
      assert {:error, %Ecto.Changeset{}} = Back.update_source_sub_reddit(source_sub_reddit, @invalid_attrs)
      assert source_sub_reddit == Back.get_source_sub_reddit!(source_sub_reddit.id)
    end

    test "delete_source_sub_reddit/1 deletes the source_sub_reddit" do
      source_sub_reddit = source_sub_reddit_fixture()
      assert {:ok, %SourceSubReddit{}} = Back.delete_source_sub_reddit(source_sub_reddit)
      assert_raise Ecto.NoResultsError, fn -> Back.get_source_sub_reddit!(source_sub_reddit.id) end
    end

    test "change_source_sub_reddit/1 returns a source_sub_reddit changeset" do
      source_sub_reddit = source_sub_reddit_fixture()
      assert %Ecto.Changeset{} = Back.change_source_sub_reddit(source_sub_reddit)
    end
  end
end
